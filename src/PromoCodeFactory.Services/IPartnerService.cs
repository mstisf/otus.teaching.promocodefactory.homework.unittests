﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace PromoCodeFactory.Services
{
    public interface IPartnerService
    {
        Task<Partner> GetPartnerByIdAsync(Guid partnerId);
        void SetPartnerNewLimit(Guid partnerId, PartnerPromoCodeLimit limit);
    }
}
